<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:variable name="courseSearch1" select="//courses/course[id='CM79']"/>
	<xsl:variable name="courseSearch2" select="//courses/course[id='EG82']"/>
	<xsl:variable name="allCourses" select="//courses/course"/>
	<xsl:variable name="allModules" select="//modules/module"/>
	<xsl:variable name="search" select="id"/>
	<xsl:variable name="preRequisites" select="prerequisite/module/*[id =$search] "/>
	<xsl:variable name="compulsoryModules" select="compulsoryFor/course/*[id = $search]"/>
	<xsl:variable name="alternativeCompulsoryModules" select="compulsoryFor/alternative/course[id = $search]"/>
	<xsl:template match="/obu">
		<html>
			<body>
				<h1>
						Course information
					</h1>
				<xsl:for-each select="$courseSearch1">
					<h2>
						<xsl:value-of select="name"/>
					</h2>
					<h2>
						<xsl:value-of select="id"/>
						<xsl:variable name="search" select="id"/>
					</h2>
					<h3>Semester 1
					</h3>
					<hr/>
					<br/>
				</xsl:for-each>
				<table border="1">
					<tr bgcolor="#9acd32">
						<th>Module Name</th>
						<th>Module UID</th>
						<th>Compulsory for</th>						
					</tr>
					<xsl:for-each select="$allModules[contains(semester,'1')] ">
						<xsl:if test="contains(. , $compulsoryModules ) ">
							<tr>
								<td>
									<xsl:value-of select="name"/>
								</td>
								<td>
									<xsl:value-of select="id"/>
								</td>
								<td>
								<br />
										Compulsory for:								
										<hr/>
									<ul>
										<xsl:for-each select="compulsoryFor/course">
											<li>
												<xsl:value-of select="id"/>
												<xsl:variable name="search" select="id"/>
												<xsl:for-each select="$allCourses[id=$search]"> -							
															<xsl:value-of select="name"/>
												</xsl:for-each>
											</li>
										</xsl:for-each>
									</ul>
									<br />
								  	
									Alternate compulsory for :								
										<hr/>
																		<ul>
										<xsl:for-each select="compulsoryFor/alternative/course">
											<li>
												<xsl:value-of select="id"/>
												<xsl:variable name="search" select="id"/>
												<xsl:for-each select="$allCourses[id=$search]"> - 
													<xsl:value-of select="name"/>
												</xsl:for-each>
											</li>
										</xsl:for-each>
									</ul>
									<br />
								</td>
								
							</tr>
						</xsl:if>
					</xsl:for-each>
				</table>
				<br/>
				<br/>
				<h3>Semester 2
					</h3>
				<hr/>
				<br/>
				<table border="1">
					<tr bgcolor="#9acd32">
						<th>Module Name</th>
						<th>Module UID</th>
						<th>Compulsory for</th>
					</tr>
					<xsl:for-each select="$allModules[contains(semester,'2')] ">
						<xsl:if test="contains(. , $compulsoryModules ) ">
							<tr>
								<td>
									<xsl:value-of select="name"/>
								</td>
								<td>
									<xsl:value-of select="id"/>
								</td>
								<td>
								<br />
										Compulsory for:								
										<hr/>
									<ul>
										<xsl:for-each select="compulsoryFor/course">
											<li>
												<xsl:value-of select="id"/>
												<xsl:variable name="search" select="id"/>
												<xsl:for-each select="$allCourses[id=$search]"> -							
															<xsl:value-of select="name"/>
												</xsl:for-each>
											</li>
										</xsl:for-each>
									</ul>
									<br />
									Alternate compulsory for :								
										<hr/>
																		<ul>
										<xsl:for-each select="compulsoryFor/alternative/course">
											<li>
												<xsl:value-of select="id"/>
												<xsl:variable name="search" select="id"/>
												<xsl:for-each select="$allCourses[id=$search]"> - 
													<xsl:value-of select="name"/>
												</xsl:for-each>
											</li>
										</xsl:for-each>
									</ul>
									<br />
								</td>
								
							</tr>
						</xsl:if>
					</xsl:for-each>
				</table>
				<br/>
				<xsl:for-each select="$courseSearch2">
					<h2>
						<xsl:value-of select="name"/>
					</h2>
					<h2>
						<xsl:value-of select="id"/>
						<xsl:variable name="search" select="id"/>
					</h2>
					<h3>Semester 1
					</h3>
					<hr/>
					<br/>
				</xsl:for-each>
				<table border="1">
					<tr bgcolor="#9acd32">
						<th>Module Name</th>
						<th>Module UID</th>
						<th>Compulsory for</th>
						
					</tr>
					<xsl:for-each select="$allModules[contains(semester,'1')] ">
						<xsl:if test="contains(. , $compulsoryModules ) ">
							<tr>
								<td>
									<xsl:value-of select="name"/>
								</td>
								<td>
									<xsl:value-of select="id"/>
								</td>
								<td>
								<br />
										Compulsory for:								
										<hr/>
									<ul>
										<xsl:for-each select="compulsoryFor/course">
											<li>
												<xsl:value-of select="id"/>
												<xsl:variable name="search" select="id"/>
												<xsl:for-each select="$allCourses[id=$search]"> -							
															<xsl:value-of select="name"/>
												</xsl:for-each>
											</li>
										</xsl:for-each>
									</ul>
									<br />
									Alternate compulsory for :								
										<hr/>
																		<ul>
										<xsl:for-each select="compulsoryFor/alternative/course">
											<li>
												<xsl:value-of select="id"/>
												<xsl:variable name="search" select="id"/>
												<xsl:for-each select="$allCourses[id=$search]"> - 
													<xsl:value-of select="name"/>
												</xsl:for-each>
											</li>
										</xsl:for-each>
									</ul>
									<br />
								</td>
								
							</tr>
						</xsl:if>
					</xsl:for-each>
				</table>
				<br/>
				<br/>
				<h3>Semester 2
					</h3>
				<hr/>
				<br/>
				<table border="1">
					<tr bgcolor="#9acd32">
						<th>Module Name</th>
						<th>Module UID</th>
						<th>Compulsory for</th>
					</tr>
					<xsl:for-each select="$allModules[contains(semester,'2')] ">
						<xsl:if test="contains(. , $compulsoryModules ) ">
							<tr>
								<td>
									<xsl:value-of select="name"/>
								</td>
								<td>
									<xsl:value-of select="id"/>
								</td>
								<td>
								<br />
										Compulsory for:								
										<hr/>
									<ul>
										<xsl:for-each select="compulsoryFor/course">
											<li>
												<xsl:value-of select="id"/>
												<xsl:variable name="search" select="id"/>
												<xsl:for-each select="$allCourses[id=$search]"> -							
															<xsl:value-of select="name"/>
												</xsl:for-each>
											</li>
										</xsl:for-each>
									</ul>
									<br />
									Alternate compulsory for :								
										<hr/>
																		<ul>
										<xsl:for-each select="compulsoryFor/alternative/course">
											<li>
												<xsl:value-of select="id"/>
												<xsl:variable name="search" select="id"/>
												<xsl:for-each select="$allCourses[id=$search]"> - 
													<xsl:value-of select="name"/>
												</xsl:for-each>
											</li>
										</xsl:for-each>
									</ul>
									<br />
								</td>							
							</tr>
						</xsl:if>
					</xsl:for-each>
				</table>
				<br/>
				</body>
		</html>
	</xsl:template>
</xsl:stylesheet>